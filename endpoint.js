const http = require('http');
const https = require('https');
const querystring = require('querystring');

const port = process.env.PORT || 8888;

const authOptions = {
    "client_id": "7eac520f-a60d-472e-8b77-5d8bcb88cc29",
    "client_secret": "2ccWaQKpho6ZJOWc7SbpxH3IjDgtL1mVG3jPx1eDmuxv85Qt854dLhlxQd4qQidc",
    "grant_type": "authorization_code",
    "code": "def5020026b4cfe0fde70555cdd64de0eb1d350901e9d5e275fa4dbfa96966508166e039dfd905adae77cfcb4711e22c65bf0aa498a3ecfb0bbd7e480b14e8a6d630563efeb5feb283f89f713b3e680a6134d00ff47d19eb7a966313b86a7329998deb220d36bd732c8703d09438668244b9b7c00d128d95cb8c79b8fefbcfe0606464a33a9d1213ba5bb5d1b07b553b584f882db7dde9ef1f5e6415ff9e262c6af137871e6c3697ce167648a9dcaed3dc4bec84089c44bc9e14c37e0f3d0b627592af0806bc8463f8ebd29d586a77b070f37f281f321bdde7d816ff221722ebb606338a243650d7340fccb6b41f3b79992e572da4023d38e12f830924598584e33aac2fe42469343fab1cb762ddf27d7265934546891402badbb41f36a382d1ee7cd69ab9570ed3b3fc8e35efbc910558db9aa84a669afa847acce5f727ae47cb2d424d2f73a05193aa2937ec460ab49fb3d4ab489074f6bfea4acd96b943f76bb8b50ea50016a74f1b06f2857a81ae8e82c5217a30167492ee0c27eaeb97fc2f83a82ee1e969d3572e3ab43535a884fcebb7b839c115c6b8dfa5f5e72b53c9ae2fffc5b95172a962976cb6333d7d6b059a7e8d067094cb4cd91ee3f22e057718c0c8aca936a50ef0fc9b4725571868be70",
    "redirect_uri": "https://warm-oasis-45315.herokuapp.com/"
}

let tokens = {};
let users = {};
let leads = {};


function makeRequest(url, options, data, callback) {
    let req = https.request(url, options, callback);

    if(data && req.write(data)) {
        console.log(`Request sended`);
    }
    req.end(() => console.log);
}

function getToken() {
    return new Promise( resolve => {
        let data = JSON.stringify(authOptions);
    
        const url = new URL("https://kmssrvk.amocrm.ru/oauth2/access_token");
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
        };
    
        makeRequest(url, options, data, res => {
            let respJson;
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            });
            res.on('end', () => {
                respJson = JSON.parse(chunks.toString());
                console.log(respJson);
                
                if(respJson && respJson.hint && respJson.hint === "Authorization code has expired") {
                    console.log('Authorization code has expired. Try get new token. . . ');
                    
                    data = JSON.stringify({
                        "client_id": authOptions.client_id,
                        "client_secret": authOptions.client_secret,
                        "grant_type": "refresh_token",
                        "refresh_token": `${tokens.refresh_token}`,
                        "redirect_uri": authOptions.redirect_uri
                    });
                    
                    makeRequest(url, options, data, res => {
                        let chunks = [];
                        res.on('data', (d) => {
                            chunks.push(d);
                        });
                        res.on('end', () => {
                            tokens = JSON.parse(chunks.toString());
                        });
                    });
                    
                } else {
                    tokens = respJson;
                }

                resolve(tokens);
                //TODO: Need reject
            });



        });
    });
}

function getUsers() {
    return new Promise( resolve => {
        const url = 'https://kmssrvk.amocrm.ru/api/v2/account?with=users&free_users=Y';
        const options = {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${tokens.access_token}`
            }
        };

        makeRequest(url, options, null, res => {
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            });
            res.on('end', () => {
                users = JSON.parse(chunks.toString());
                resolve(users);
                //TODO: Need reject
            });
        })
    });
}

function getLeads() {
    return new Promise( resolve => {
        const url = 'https://kmssrvk.amocrm.ru/api/v2/leads';
        const options = {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${tokens.access_token}`
            }
        };

        makeRequest(url, options, null, res => {
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            });
            res.on('end', () => {
                leads = JSON.parse(chunks.toString());
                resolve(leads);
                //TODO: Need reject
            });
        });
    });
}

function sendAcceptUnsortedToCrm(userId, unsortedId) {
    return new Promise( resolve => {
        const data = JSON.stringify({
            "accept": [ unsortedId ],
            "user_id": userId,
            "status_id": "142"
        });

        const url = 'https://kmssrvk.amocrm.ru/api/v2/incoming_leads/accept';
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokens.access_token}`
            }
        };

        makeRequest(url, options, data, res => {
            let chunks = [];
            res.on('data', (d) => {
                chunks.push(d);
            });
            res.on('end', () => {
                accepted = JSON.parse(chunks.toString());
                resolve(accepted);
                //TODO: Need reject
            });
        });
    });
}

function acceptUnsorted(unsortedUID) {
    return new Promise(async resolve => {
        let availableUsers = Array.prototype;

        if(users) {
            for(const user in users._embedded.users) {
                availableUsers.push({
                    id: user,
                    leads_count: 0
                });
            }
        }

        leads._embedded.items.forEach(lead => {
            availableUsers.forEach(user => {
                if(user.id === lead.responsible_user_id)
                    user.leads_count++;
            })});

        const neededUser = availableUsers.reduce( (acc, cur) => acc.leads_count < cur.leads_count ? acc : cur );

        await sendAcceptUnsortedToCrm(neededUser.id, unsortedUID).then(resolve);
    });
}

async function processRequests(req, resp) {
    if(!tokens || (!tokens.access_token && !tokens.refresh_token)) {
        await getToken();
    }

    let data = []
    req.on('data', chunk => data.push(chunk));
    
    req.on('end', async () => {
        const decoded = decodeURIComponent(data.toString());
        decoded.replace('+', ' ');
        const unsortedParsed = querystring.parse(decoded)
        
        await getUsers();
        await getLeads();
        const unsortedUID = unsortedParsed['unsorted[add][0][uid]'];
        await acceptUnsorted(unsortedUID);

        resp.statusCode = 200;
        resp.end();
    });
}

const server = http.createServer(processRequests);

server.listen(port, () => {
    console.log(`Endpoint running at ${port}`);
});
